import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": city + " " + state}
    url = "http://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = response.content
    parsed_json = json.loads(content)
    picture = parsed_json["photos"][0]["src"]["original"]
    return {"picture_url": picture}


def get_lat_long(location):
    base_url = "https://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{location.city},{location.state.abbreviation},USA",
        "appid": OPEN_WEATHER_API_KEY,
    }
    response = requests.get(base_url, params=params)
    # print("get_lat_long response status code:", response.status_code)
    # print("get_lat_long response content:", response.content)
    parsed_json = json.loads(response.content)
    return {
        "latitude": parsed_json[0]["lat"],
        "longitude": parsed_json[0]["lon"],
    }


def get_weather_data(location):
    lat_long = get_lat_long(location)
    base_url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "lat": lat_long["latitude"],
        "lon": lat_long["longitude"],
        "appid": OPEN_WEATHER_API_KEY,
        "units": "imperial",
    }
    response = requests.get(base_url, params=params)
    # print("get_weather_data response status code:", response.status_code)
    # print("get_weather_data response content:", response.content)
    parsed_json = json.loads(response.content)
    weather_data = {
        "temp": parsed_json["main"]["temp"],
        "description": parsed_json["weather"][0]["description"],
    }
    return weather_data
